# Guild Wars 2 Trading Post Calculator
A small tool for calculating things regarding the Trading post.

## Usage
The binary created from the installation will be called `g2t`

### Flip calc
You can calculate profits for a trade (`g2t`):<br>
![Crafting calculator](https://i.imgur.com/1UTVqtt.png)

### Relist Calc
If you are buying a larger amount of a low cost item your buy orders can sometimes get filled randomly.
This is either because of orders filling before you put in another one or someone outbids you and a seller sells more items than the person outbidding you listed for.
In these cases you can end up with half filled orders and you might want to outbid your competitors, but it can be hard keeping track of the amount of items in all your orders that havent gone through yet.

So with the `-r` flag you can do this (`g2t -r`):<br>
![Relist calc](https://i.imgur.com/BghpQa1.png)

### Percentage calc
When you are trading on outside-of-game forums, such as the Overflow discord server, you might want to check how much gold 85% of highest bid/ask is.

To do this, use the `-p` flag (`g2t -p`):<br>
![Percentage calc](https://i.imgur.com/oSn4ygB.png)

## Installing
1. `git clone https://gitlab.com/mWalrus/gw2tpc.git`
2. `cargo build --release`
3. `sudo install -s -Dm755 $PWD/target/release/gw2tpc /usr/bin/g2t`
