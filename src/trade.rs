use anyhow::Result;

use crate::price::Price;
use crate::colored::*;
use crate::dialoguer::{theme::ColorfulTheme, Input};
use crate::console::Style;
use std::fmt;

pub struct Flip {
    buy_price: Price,
    sell_price: Price,
    listing_fee: Price,
    exchange_fee: Price,
    total_fees: Price,
    quantity: i32,
    profit: Price,
}

#[derive(PartialEq)]
enum PromptStyle {
    Gold,
    Silver,
    Copper,
    Quantity
}

impl Flip {
    pub fn new() -> Flip {
        print_heading("Enter buy/craft price");
        let (buy_gold, buy_silver, buy_copper) = prompt_price().unwrap();
        let buy_price = Price::new(buy_gold, buy_silver, buy_copper);

        print_heading("Enter sell price");
        let (sell_gold, sell_silver, sell_copper) = prompt_price().unwrap();
        let sell_price = Price::new(sell_gold, sell_silver, sell_copper);

        print_heading("Quantity");
        let quantity = prompt(
            "Enter quantity",
            PromptStyle::Quantity,
        ).unwrap().parse::<i32>().unwrap_or(1);

        // fee calculation
        let listing_fee = sell_price * 0.10;
        let exchange_fee = sell_price * 0.05;
        let total_fees = listing_fee + exchange_fee;

        // calculate base profit
        let profit = sell_price - buy_price;
        // subtract the fees to get the net profit
        let profit = profit - total_fees;

        Flip {
            buy_price,
            sell_price,
            listing_fee,
            exchange_fee,
            total_fees,
            quantity,
            profit
        }
    }

    fn total_price(&self, price: Price) -> Price {
        price * self.quantity
    }

    fn percentage(&self) -> String {
        let quotient = self.profit / self.buy_price;
        // round to 3 decimals and shift decimal by two places. (eg. 0.149 => 14.9)
        format!("{}%", f32::trunc(quotient * 1000.00) / 10.00)
    }
}

impl fmt::Display for Flip {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        print_heading("Results");
        write!(f, "{}\t\t{}\n", "Buy price:".bold(), self.total_price(self.buy_price))?;
        write!(f, "{}\t\t{}\n", "Sell price:".bold(), self.total_price(self.sell_price))?;
        write!(f, "{}\t\t{}\n", "Listing fee:".bold(), self.total_price(self.listing_fee))?;
        write!(f, "{}\t\t{}\n", "Exchange fee:".bold(), self.total_price(self.exchange_fee))?;
        write!(f, "{}\t\t{}\n", "Total fees:".bold(), self.total_price(self.total_fees))?;

        let total_profit = self.total_price(self.profit);
        let profit_spacing = match total_profit.is_positive() {
            true => "\t\t\t".to_string(),
            false => format!("\t\t{}", " ".repeat(7)), // 7 spaces is one "\t" - 1 space
        };
        write!(f, "{}{}{}", "Profit:".bold(), profit_spacing, self.total_price(self.profit))?;

        let percentage = self.percentage();
        match percentage.starts_with("-") {
            true => write!(f, " ({})", percentage.bold().on_red().bright_white()),
            false => write!(f, " ({})", percentage.bold().on_truecolor(6, 177, 43).bright_white()),
        }

    }
}

fn prompt_price() -> Result<(String, String, String)> {
    let gold = prompt("Gold", PromptStyle::Gold)?;
    let silver = prompt("Silver", PromptStyle::Silver)?;
    let copper = prompt("Copper", PromptStyle::Copper)?;
    Ok((gold, silver, copper))
}

fn prompt(msg: &str, ps: PromptStyle) -> Result<String> {
    // check the passed type and decide style
    let prompt_style: Style = match ps {
        PromptStyle::Gold => Style::new().yellow().bold(),
        PromptStyle::Silver => Style::new().white().bold(),
        PromptStyle::Copper => Style::new().color256(130).bold(),
        PromptStyle::Quantity => Style::new().magenta().bold(),
    };

    let theme = ColorfulTheme {
        prompt_style,
        ..ColorfulTheme::default()
    };

    let input = Input::with_theme(&theme)
        .with_prompt(
            format!(
                "{}",
                msg.bold(),
            )
        )
        .validate_with({
            move |input: &String| -> Result<(), &str> {
                match input.parse::<i32>() {
                    Ok(_) => {
                        let parsed = input.parse::<i32>().unwrap();
                        if parsed < 0 {
                            Err("Number cannot be negative.")
                        }
                        else if (ps == PromptStyle::Silver || ps == PromptStyle::Copper) && parsed > 99 {
                            Err("Number has to be lower than 100.")
                        }
                        else {
                            Ok(())
                        }
                    },
                    Err(_) => {
                        Err("This is not a number, try again.")
                    }
                }
            }
        })
        .default("1".to_string())
        .interact_text()?;
    Ok(input)
}

fn print_heading(msg: &str) {
    println!("{}{}", msg.bold().blue(), ":".bold().blue());
}

// Price percentage calculation for when trading on discord
pub fn price_percentage() -> Result<()> {
    print_heading("Enter full price");
    let (gold, silver, copper) = prompt_price()?;
    let base_price = Price::new(gold, silver, copper);

    print_heading("Percentage");
    let percentage = prompt("Percentage of price", PromptStyle::Quantity)?
        .parse::<f32>()?;
    let percentage = percentage / 100.0;

    print_heading("Quantity");
    let quantity = prompt("Quantity", PromptStyle::Quantity)?
        .parse::<f32>()?;

    let price_single = base_price * percentage;
    let price_all = price_single * quantity;

    print_heading("Results");
    println!("{} {}", "One:".bold(), price_single);
    println!("{} {}", "All:".bold(), price_all);
    Ok(())
}

pub fn relist_calc() -> Result<()> {
    print_heading("Amount of buy orders");
    let buy_orders = prompt("Amount", PromptStyle::Quantity)?.parse::<u16>()?;

    let mut total = 0;

    print_heading("Quantity in buy orders");
    for i in 0..buy_orders {
        let q = prompt(&(i + 1).to_string(), PromptStyle::Quantity)?;
        total += q.parse::<u16>()?;
    }

    print_heading("Results");
    if total > 250 {
        let stack_amount =  total / 250;
        let remainder = total % 250;
        println!("{} {} ({}x 250 + {})", "Total:".bold(), total, stack_amount, remainder);
    }
    else {
        println!("{} {}", "Total:".bold(), total);
    }

    Ok(())
}
