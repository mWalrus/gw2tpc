use crate::coin::Coin;
use crate::colored::*;
use std::{
    fmt,
    ops,
    convert::From,
};


#[derive(Copy, Clone)]
pub struct Price {
    gold: Option<Coin>,
    silver: Option<Coin>,
    copper: Coin,
    positive: bool,
}

impl Price {
    pub fn new(gold: String, silver: String, copper: String) -> Price {
        Price {
            gold: Some(Coin::new(&gold, 'g')),
            silver: Some(Coin::new(&silver, 's')),
            copper: Coin::new(&copper, 'c'),
            positive: true,
        }
    }

    fn raw_price(&self) -> i32 {
        let total: i32 = match self.gold.is_some() {
            // multiply by 10000 since the gold amount is
            // displaced by 4 zeroes in the raw price
            true => (self.gold.unwrap().amount as i32 * 10000),
            false => 0,
        };
        let total = match self.silver.is_some() {
            // same as above we displace the silver amount by 2 zeroes
            true => total + (self.silver.unwrap().amount as i32 * 100),
            false => total,
        };
        // make resulting number positive or not
        if self.is_positive() {
            total + self.copper.amount as i32
        }
        else {
            - (total + self.copper.amount as i32)
        }
    }

    pub fn is_positive(&self) -> bool {
        self.positive
    }
}

impl ops::Add for Price {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let result = self.raw_price() + other.raw_price();
        Price::from(result.to_string())
    }
}

impl ops::Sub for Price {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        let result = self.raw_price() - other.raw_price();
        Price::from(result.to_string())
    }
}

impl ops::Mul<i32> for Price {
    type Output = Self;

    fn mul(self, multiplier: i32) -> Self {
        let product = self.raw_price() * multiplier;
        Price::from(product.to_string())
    }
}

impl ops::Div for Price {
    type Output = f32;

    fn div(self, divisor: Price) -> f32 {
        self.raw_price() as f32 / divisor.raw_price() as f32
    }
}

impl ops::Mul<f32> for Price {
    type Output = Self;

    fn mul(self, multiplier: f32) -> Self {
        let product = self.raw_price() as f32 * multiplier;
        let product = product as i32;
        Price::from(product.to_string())
    }
}

impl From<String> for Price {
    fn from(mut s: String) -> Price {
        // remove the leading minus sign and mark the price as negative instead
        let mut positive = true;
        if s.starts_with("-") {
            positive = false;
            s = s.replace("-", "");
        }

        // insert a leading zero to format the amount
        if s.len() == 3 || s.len() == 1 {
            s = "0".to_string() + &s;
        }

        // grab the gold amount
        let gold = match s.len() > 4 {
            true => Some(Coin::new(&s[0..s.len() - 4], 'g')),
            false => None,
        };

        // grab the silver amount
        let silver = match s.len() > 2 {
            true => Some(Coin::new(&s[&s.len()-4..s.len()-2], 's')),
            false => None,
        };
        // grab the copper amount
        let copper = Coin::new(&s[&s.len()-2..], 'c');

        Price {
            gold,
            silver,
            copper,
            positive,
        }
    }
}

impl fmt::Display for Price {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // appending the minus to the output
        if !self.positive {
            write!(f, "{}", "-".bold().red())?;
        }
        // conditional writes if the fields contain a value
        if self.gold.is_some() {
            write!(f, "{} ", &self.gold.unwrap())?;
        }
        if self.silver.is_some() {
            write!(f, "{} ", &self.silver.unwrap())?;
        }
        write!(f, "{}", &self.copper)
    }
}
