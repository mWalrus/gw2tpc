extern crate dialoguer;
extern crate console;
extern crate colored;
extern crate serde;
extern crate clap;
extern crate anyhow;

mod trade;
mod price;
mod coin;

use trade::{Flip, price_percentage, relist_calc};
use clap::{App, Arg};

fn main() {
    let arg_matches = App::new("GW2TPC")
        .arg(Arg::with_name("percentage_calc")
            .short("p")
            .help("Calculate how much any given percentage of the original price is.")
            .takes_value(false))
        .arg(Arg::with_name("relist_quantity")
            .short("r")
            .help("Calculate how many items you need to relist (for scattered buy orders)")
            .takes_value(false)).get_matches();

    if arg_matches.is_present("percentage_calc") {
        price_percentage().unwrap();
    }
    else if arg_matches.is_present("relist_quantity") {
        relist_calc().unwrap();
    }
    else {
        println!("{}", Flip::new());
    }
}

