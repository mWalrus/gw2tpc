use crate::colored::*;
use std::fmt;

#[derive(Copy, Clone)]
pub struct Coin {
    pub amount: i16,
    suffix: char,
}

impl Coin {
    pub fn new(amount: &str, suffix: char) -> Coin {
        Coin {
            amount: amount.parse::<i16>().unwrap(),
            suffix,
        }
    }
}

impl fmt::Display for Coin {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let formatted = format!("{}{}", self.amount, self.suffix);
        match self.suffix {
            'g' => write!(f, "{}", formatted.bold().yellow()),
            's' => write!(f, "{}", formatted.bold().white()),
            'c' => write!(f, "{}", formatted.bold().truecolor(184, 115, 51)),
            _ => write!(f, "{}", "invalid".bold()),
        }
    }
}
